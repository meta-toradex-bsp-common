# Bringup support for Toradex SMARC iMX8MP series SoMs
SRC_URI:toradex-smarc-imx8mp = "git://git.toradex.com/u-boot-toradex.git;protocol=https;branch=${SRCBRANCH}"
SRCREV:toradex-smarc-imx8mp = "${AUTOREV}"
SRCBRANCH:toradex-smarc-imx8mp = "toradex_u-boot-2024.07_smarc-imx8mp-bringup"
